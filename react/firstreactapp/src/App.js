import logo from './logo.svg';
import './App.css';
import {Message} from "./components/Message";

function App() {
  return (
    <div className="App">
        <header className="App-header">
            <Message text="Text"/>
        </header>
    </div>
  );
}

export default App;
