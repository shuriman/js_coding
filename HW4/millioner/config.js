const config = {
    position: 0,
    questions: ['Сколько букв в слове привет?', 
                'Сколько букв в слове слово?', 
                'Сколько букв в слове мир?',
                'Сколько букв в слове пока?',
                'Сколько букв в слове мама?'],
    variants: [['1' , '2', '3', '6'], ['1' , '2', '3', '5'], ['1' , '2', '3', '4'] , ['1' , '2', '3', '4'], ['1' , '4', '3', '2']],
    right_answer: ['6', '5', '3', '4', '4'],
    score: 0

}