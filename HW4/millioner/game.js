let game = {
    run(){
        
        while(config.position < config.questions.length){
            const availableDirections = config.variants[config.position];
            let answer = prompt(config.questions[config.position]+ " варианты ответа: " + availableDirections);

            if(isNaN(answer) || answer == null){
                game.result();
                return null;
            }

            if(answer == config.right_answer[config.position]){
                alert('Правильный ответ!');
                config.position += 1;
                config.score += 1;
            }else{
                alert('Неправильный ответ!');
                game.result();
                return;
            }
        }
        alert('Поздравляю вы победитель!');
        game.result();
    },

    result(){
        console.log('Ваш счет: ' + config.score);
        console.log('Чтобы начать введите game.run()');
    },

    init(){
        
        console.log('Приветствую вас на игре "Кто хочет стать миллионером"');
        console.log('5 вопросов 4 варианта ответа каждый!');
        console.log('Чтобы начать введите game.run()');
        //game.run();
    }

};

game.init();