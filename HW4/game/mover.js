let mover = {
    getDirection(){
        const availableDirections = [1, 2, 3, 4, 5, 6, 7, 8];
        while(true){
            let direction = parseInt(prompt('Введите 1, 2, 3, 4, 5, 6, 7 или 8, куда хотите переместиться, Отмена для выхода. '));

            if(isNaN(direction)){
                return null;
            }

            if(!availableDirections.includes(direction)){
                alert('Для перемещения необходимо ввести одно из чисел 1, 2, 3, 4, 5, 6, 7 или 8');
                continue;
            }

            return direction;
        }
    },

    getNextPosition(direction){
        const nextPosition = {
            x: player.x,
            y: player.y
        };

        switch(direction){
            case 1:
                nextPosition.x--;
                nextPosition.y--;
                break;
            case 2:
                nextPosition.y++;
                break;
            case 3:
                nextPosition.x++;
                nextPosition.y--;
                break;                
            case 4:
                nextPosition.x--;
                break;
            case 5:
                nextPosition.x--;
                nextPosition.y++;
                break;                
            case 6:
                nextPosition.x++;
                break;
            case 7:
                nextPosition.x++;
                nextPosition.y++;
                break;                
            case 8:
                nextPosition.y--;
                break;
        }

        return mover.checkPosition(nextPosition);
    },

    checkPosition(position){
        if(position.x < 0)
            position.x = 0;
        else if(position.x > config.rowsCount)
            position.x = config.rowsCount;

        if(position.y < 0)
            position.y = 0
        else if(position.y > config.colsCount)
            position.y = config.colsCount;
        
            return position;

    }
};